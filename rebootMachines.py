import os
import sys
import argparse
import subprocess
import time
import getpass

## This script does the following:
## 1. Remotely reboots Windows machines based on their host name
##    Each hostname has a level in the name.
## 2. For each machine, check for it to be up for 5 minutes then checks
##    if the services that are scheduled to restart on reboot start successfully.
## Dependencies: pslist and psshutdown installed on all machines


# global variables
#
all_levels = ['br212','br213','br214','br215','br216','br217','br218','br219','br220b1','br220b2','br221b1','br221b2','br221','br222b2','br222','br223']

def error_checking():

    auth_user = 'admin'
    current_user = getpass.getuser()
    if current_user != auth_user:
        print("ERROR: This tool can only be run as " + auth_user)
        sys.exit(1)
        
    if 'win' not in sys.platform:
        print("ERROR: This tool runs on Windows only.\n")
        sys.exit(1)

################################################################
##
## Function: checkLevels
##
## Arguments: rootPath = string contains root path to search
##            ext = string contains file extension
##
## Returns: list of servers hostnames to reboot
################################################################
def checkLevels(levels_to_check):
    # Level error checking
    level_errors = True
    
    for level in levels_to_check:
        if level not in all_levels:
            print("ERROR: level" + level + "is not a valid level.")
            level_errors = False
            
    return level_errors    
    
################################################################
##
## Function: reboot_servers
##
## Arguments: machines_to_reboot = list of machine names to reboot
##            verbosity = flag to show more messaging (1 = enable, 0 = disable)
##
## Returns: list
################################################################
def reboot_servers(machines_to_reboot,verbosity=0):
    # Spawn reboot of each server
    
    for hostname in machines_to_reboot:
        rebootCmd = ['psshutdown.exe','\\\\' + hostname,'-t','0','-r']
        
        print('Rebooting ' + hostname + ' ...')
        if verbosity:
            print( time_stamp() + ' DEBUG: Reboot command: ' + ' '.join(rebootCmd) + "\n")
        
        rebootCmd = subprocess.Popen(rebootCmd,stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        rebootout = rebootCmd.communicate()
        rebootreturncode = rebootCmd.returncode
        
    return True 
        
################################################################
##
## Function: post_reboot
##
## Arguments: machines_to_reboot = list of machine names to reboot
##            verbosity = flag to show more messaging
##
## Returns: True
################################################################
def post_reboot(machines_to_reboot,verbosity):
    # Monitor machines for uptime (up for 5 minutes or more)
    # Once all is well, check the diffusion services are running
    
    failure_post_reboot = []
    completed_post_reboot = []
    machines_to_reboot.sort()
    recheck_freq_in_secs = 30 # In seconds
    
    print("\nChecking servers after reboot ...\n")
    
    while machines_to_reboot != completed_post_reboot:
    
        # Wait is here because we should wait for the reboot to complete
        # which can take time before checking if its back online
        print("\n" + time_stamp() + ": Waiting for " + str(recheck_freq_in_secs) + " seconds .." + "\n")
        if verbosity:
            print('DEBUG: Servers checked so far:' + str('\n'.join(completed_post_reboot)) + "\n")
            
        time.sleep(recheck_freq_in_secs)
            
        for hostname in machines_to_reboot:
            pslist_found = []
            found_idle_time = False
            
            # If a machine has been checked, check the next machine.
            if hostname in completed_post_reboot:
                print('  ' + hostname + ' is reboot is complete.' + "\n")
                continue
            
            # Otherwise, remotely check if machine is up
            response = subprocess.Popen(['ping','-n','1',hostname],stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            pingfound = response.communicate()
            returncode = response.returncode
           
            if returncode == 0: # Machine pings.
                if verbosity:
                    print("\n" + time_stamp() + ':  DEBUG: ' + hostname + ' is pingable!' + "\n")
                
                # If it's up, how long?
                # Look in pslist output for Idle time
                process_cmd = subprocess.Popen(['pslist','-c','\\\\' + hostname],stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                pslist_found = process_cmd.communicate()
                idleTime = ''
                
                for pslist_line in pslist_found[0].split("\n"):
                    
                    if "Idle " in pslist_line and not found_idle_time:
                        idledata = pslist_line.split()
                        idletime = idledata[-1] # 771:45:50.951
                      
                        try:
                            idlehours = int(idletime.split(':')[0]) # 771
                            idlemins = int(idletime.split(':')[1])  # 45
                        except ValueError:
                            print("ERROR: Unable to cast time to integer for " + hostname + ".\n" + "Line read: " + pslist_line)
                            break
                        
                        # If its not been up long enough, check again in a bit..
                        if idlehours == 0 and idlemins < 5:
                            if idlemins > 1:
                                minstring = str(idlemins) + ' minutes'
                            elif idlemins == 0:
                                minstring = 'less than a minute'
                            else:
                                minstring = str(idlemins) + ' minute'
                            print('  ' + hostname + " is up for " + minstring + '. Please wait...')
                            found_idle_time = True
                        elif idlehours != 0:
                            # The reboot didnt happen yet. check again in a bit..
                            print('  ' + hostname + ' has not been rebooted yet. (uptime: ' + str(idlehours) + ' hours), Please wait...')
                            found_idle_time = True
                        else:
                            # Machine is ready for service checks
                            print('  ' + hostname + " is up for " + str(idlemins) + ' minutes. Checking services ...' + "\n")
                            servicestate = check_diff_services(hostname,verbosity)
                            
                            if servicestate == True:
                                print('  ' + hostname + ': reboot is complete and services are running. (uptime: ' + str(idlehours) + 'hrs, ' + str(idlemins) + ' mins )')
                                found_idle_time = True
                            else:
                                print('ERROR: ' + hostname + ': reboot is complete but one or more services are NOT running. Please investigate.' + "\n")
                                failure_post_reboot.append(hostname)
                                found_idle_time = True
                                
                            completed_post_reboot.append(hostname) # Mark as checked
            else:
                if verbosity:
                    print('  ' + hostname + ' is offline! Please wait...' + "\n")
            
        completed_post_reboot.sort()
        
        if verbosity:
            print('machines_to_reboot: ' + ' '.join(machines_to_reboot) + "\n" + 'completed_post_reboot: ' + ' '.join(completed_post_reboot) + "\n")

        # Show what machines are left to be checked
        if machines_to_reboot != completed_post_reboot:
            print('  Waiting for the following machines: ' + str(','.join(list_diff(machines_to_reboot,completed_post_reboot))))
            
    if len(failure_post_reboot) != 0:
        print("ERROR: One or more machines failed check after reboot. Please investigate.")
        for mn in failure_post_reboot:
            print(mn + "\n")
        print("Other machines rebooted successfully.\n")
    else:
        print("\n" + time_stamp() + ": All machines defined have been rebooted successfully.")
        print("\n" + str(','.join(completed_post_reboot)) + "\n")

    return True 


################################################################
##
## Function: check_services
##
## Arguments: host = hostname of Windows machine to check
##            verbosity = lag to show more messaging
##
## Returns: True or False
################################################################
def check_services(host,verbosity):
    # Check status of diffusion services on machine after 5 minutes of uptime
    
    returnstate = True
    serviceChecked = 0
    
    services = ['MyService','MyService2']
    
    for servicename in services:
        service_cmd = ['sc','\\\\' + host,'query',servicename]
        if verbosity:
            print('  DEBUG: CMD: ' + ' '.join(service_cmd) + "\n")
        serviceprocess = subprocess.Popen(service_cmd,stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        scfound = serviceprocess.communicate()
        
        # Report error if the status is NOT 'RUNNING'
        for scline in scfound[0].split("\n"): # Read STDOUT
            if 'STATE              :' in scline:
                servicestate = scline.split()
                if str(servicestate[-1]) == 'RUNNING':
                    print('  ' + host + ': ' + servicename + ' is \'' + servicestate[-1] + '\'')
                    serviceChecked += 1
                else:
                    print('ERROR: ' + host + ': ' + servicename + ' is \'' + servicestate[-1] + '\'')
                    returnstate = False

    if serviceChecked != 2:
        returnstate = False
        
    return returnstate 

################################################################
##
## Function: time_stamp
##
## Description: Generate date/time stamp for current time
##
## Arguments: none
##
## Returns: date/time stamp e.g. 2019-10-01 14:31:46
################################################################
def time_stamp():
    localtime = time.localtime()
    return str(time.strftime("%Y-%m-%d %H:%M:%S ", localtime))


################################################################
##
## Function: list_diff
##
## Description: Determine differences of two lists
##
## Arguments: li1 = List 1
##            li1 = List 2
##
## Returns: list that contains differences
################################################################
def list_diff(li1, li2): 
    return (list(set(li1) - set(li2))) 
  
    
if __name__ == "__main__":

    # Configure argument parser
    parser = argparse.ArgumentParser()
    parser.add_argument("-levels",help="Specify V6 levels e.g. br220b1 or br222 etc., comma delimited is acceptable. If no levels are specified, all machines for valid levels will be rebooted.")
    parser.add_argument("-verbose",help="Show more output",action="store_true")
    args = parser.parse_args()

    machineHead = 'winsrv'
    levels_to_check = []
    machinesToReboot = []
    
    error_checking()
    
    if args.levels is None:
        # No levels were specified. Reboot ALL machines
        levels_to_check = all_levels
    else:
        # reboot machines for levels specified only
        if not ',' in args.levels: # one level specified
            levels_to_check.append(args.levels)
        else:
            levels_to_check = args.levels.split(',')
            
        if len(levels_to_check) == 0:
            print("ERROR: No levels specified.")
            sys.exit(1)

        result = checkLevels(levels_to_check)
        if result == False:
           sys.exit(1)
        
    # Form machine host name list to reboot
    for level in levels_to_check:
        machinesToReboot.append(machineHead + level)
        
    print("Machines to reboot: " + ' '.join(machinesToReboot) + "\n")
        
    verbosemode = args.verbose
    reboot_servers(machinesToReboot,verbosemode)
    post_reboot(machinesToReboot,verbosemode)
